using Domain.Char;
using System;
using System.Collections.Generic;
using Xunit;

namespace Test
{
    public class InteractionThree
    {
        [Fact]
        public void InteractionThree1_2_3_4()
        {
            var melee = new Melee();
            var ranged = new Ranged();

            ranged.Move(20);
            melee.Move(1);

            ranged.DoDamage(melee);

            Assert.True(melee.Health == melee.MaxHealth - ranged.Damage, $"ranged Damage Error...");

            ranged.DoDamage(melee);
            ranged.DoDamage(melee);
            ranged.DoDamage(melee);
            ranged.DoDamage(melee);
            ranged.DoDamage(melee);
            ranged.DoDamage(melee);
            ranged.DoDamage(melee);
            Assert.True(melee.Health == 0, $"melee Health bellow Zero...");

            Assert.True(!melee.Alive, $"melee is Alive Error...");

            melee.Heal(melee);
            Assert.False(melee.Health > 0, $"melee is Alive Error...");
            Assert.True(!melee.Alive, $"melee is Alive Heal Error...");

            var melee2 = new Melee();
            melee2.Move(19);

            melee2.DoDamage(ranged);

            ranged.Heal(ranged);
            ranged.Heal(ranged);
            ranged.Heal(ranged);
            ranged.Heal(ranged);
            ranged.Heal(ranged);
            Assert.True(ranged.Health <= ranged.MaxHealth, $"ranged Heal Above max Error...");
        }
    }
}
