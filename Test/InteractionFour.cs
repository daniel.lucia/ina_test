using Domain.Char;
using System;
using System.Collections.Generic;
using Xunit;

namespace Test
{
    public class InteractionFour
    {
        [Fact]
        public void InteractionFour1_2_3_4()
        {
            var melee = new Melee();
            var ranged = new Ranged();
            var ranged2 = new Ranged();

            ranged.Move(20);
            melee.Move(1);

            Assert.True(ranged.Factions.Count == 0, $"ranged Belong no faction Error...");
            Assert.True(melee.Factions.Count == 0, $"melee Belong no faction Error...");


            ranged.JoinFaction(IBelongToFactions.FactionsType.Faction3);
            ranged.JoinFaction(IBelongToFactions.FactionsType.Faction2);
            ranged.LeaveFaction(IBelongToFactions.FactionsType.Faction2);

            melee.JoinFaction(IBelongToFactions.FactionsType.Faction1);
            melee.JoinFaction(IBelongToFactions.FactionsType.Faction2);
            Assert.True(ranged.Factions.Count == 1, $"ranged Belong no faction Error Add/remove...");
            Assert.True(melee.Factions.Count == 2, $"melee Belong no faction Error Add/remove...");

            
            Assert.True(!melee.IsMyAlly(ranged), $"melee Allies faction Error not allies...");

            ranged.JoinFaction(IBelongToFactions.FactionsType.Faction2);
            Assert.True(melee.IsMyAlly(ranged), $"melee Allies faction Error we are allies...");

            ranged.DoDamage(melee);
            Assert.True(melee.Health == melee.MaxHealth, $"ranged doDamage Error, cannot hurt alies...");

            ranged2.DoDamage(melee);
            Assert.True(melee.Health == melee.MaxHealth - ranged2.Damage, $"ranged2 doDamage Error, this can hurt enemies...");

            ranged.Heal(melee);
            ranged.Heal(melee);
            ranged.Heal(melee);
            ranged.Heal(melee);
            ranged.Heal(melee);

            Assert.True(melee.Health == melee.MaxHealth, $"ranged heal error, this can heal allies...");
        }
    }
}
