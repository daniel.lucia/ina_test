using Domain.Char;
using System;
using System.Collections.Generic;
using Xunit;

namespace Test
{
    public class InteractionTwo
    {
        [Fact]
        public void InteractionTwo1_2_3()
        {
            var melee = new Melee();
            var ranged = new Ranged();

            ranged.Move(20);
            melee.Move(19);

            ranged.DoDamage(ranged);
            Assert.True(ranged.Health == ranged.MaxHealth, $"ranged Damage Error...");

            melee.DoDamage(ranged);
            melee.Heal(ranged);
            Assert.True(ranged.Health == ranged.MaxHealth - melee.Damage, $"melee Heal Enemy Error...");

            melee.LevelUp();
            melee.LevelUp();
            melee.LevelUp();
            melee.LevelUp();
            melee.LevelUp();
            melee.LevelUp();

            ranged.Heal(ranged);
            ranged.Heal(ranged);
            ranged.Heal(ranged);
            ranged.Heal(ranged);

            ranged.DoDamage(melee);
            Assert.True(melee.Health == melee.MaxHealth - (ranged.Damage * .5), $"melee Redeced .5 damage error Error...");

            Assert.True(melee.DamageByLevelFactor(ranged.Level) == 1.5, $"melee Redeced 1.5 damage error Error...");
        }
    }
}
