using Domain.Char;
using System;
using System.Collections.Generic;
using Xunit;

namespace Test
{
    public class InteractionFive
    {
        [Fact]
        public void InteractionFive1_2_3_4_5()
        {
            var melee = new Melee();
            var tree = new Tree();

            melee.LevelUp();
            melee.LevelUp();
            melee.LevelUp();

            melee.DoDamage(tree);
            melee.DoDamage(tree);

            //it's impossible to heal a tree
            //melee.Heal(tree);

            //it's impossible be damaged by a tree
            //tree.DoDamage(melee);

            Assert.True(!tree.Alive, $"Tree may be destroyed...");
        }
    }
}
