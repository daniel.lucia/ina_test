﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Char
{
    public class Ranged : Character
    {
        public Ranged(int level = 1)
            :base(20, 200, level)
        {
        }
    }
}
