﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Char
{

    public class Character : IIdentifier, ICanReceiveDamage, ICanLevelUp, ICanBeHealed, ICanDoDamage, IBelongToFactions, ICanHeal, IPosition, IMoveble
    {
        public Guid Id { get; }
        public int MaxRange { get; private set; } = 0;
        public double Damage { get; private set; } = 0;
        public double MaxHealth { get; private set; }
        public double Health { get; private set; }
        public int Level { get; private set; }
        public int Metter { get; private set; }
        public bool Alive => Health > 0;
        public ICollection<IBelongToFactions.FactionsType> Factions { get; private set; }


        public Character(int maxRange, double damage, int level)
        {
            Id = Guid.NewGuid();
            Health = 1000;
            MaxHealth = Health;
            Level = level;
            MaxRange = maxRange;
            Damage = damage;
            Factions = new List<IBelongToFactions.FactionsType>();
        }

        public void ReceiveDamage(double damage)
        {
            Health -= damage;
            if (Health < 0) Health = 0;
        }

        public void Healed(double healed)
        {
            Health += healed;
            if (Health > MaxHealth) Health = MaxHealth;
        }

        public bool IsMyAlly(IBelongToFactions factionUnity)
        {
            return factionUnity.Factions.Any(x => Factions.Any(y => y.Equals(x)));
        }

        public void DoDamage(ICanReceiveDamage damagedUnity)
        {
            if (!Alive) return;

            var canBeDamaged = false;
            var computedDamage = Damage;

            if (damagedUnity is IPosition)
                canBeDamaged = IsInRange(damagedUnity as IPosition);

            if (canBeDamaged && damagedUnity is IIdentifier)
                canBeDamaged = Id != ((IIdentifier)damagedUnity).Id;

            if (canBeDamaged && damagedUnity is IBelongToFactions)
                canBeDamaged = !((IBelongToFactions)damagedUnity).IsMyAlly(this);

            if (canBeDamaged && damagedUnity is ICanLevelUp)
                computedDamage = computedDamage * DamageByLevelFactor(((ICanLevelUp)damagedUnity).Level);

            if (canBeDamaged)
                damagedUnity.ReceiveDamage(computedDamage);
        }

        public void Heal(ICanBeHealed healedUnity)
        {
            if (!Alive) return;

            double healed = Damage * .3;

            var canBeHealed = false;

            if (healedUnity is IIdentifier)
                canBeHealed = Id == ((IIdentifier)healedUnity).Id;

            if (healedUnity is IBelongToFactions)
                canBeHealed = ((IBelongToFactions)healedUnity).IsMyAlly(this);

            if (canBeHealed)
                healedUnity.Healed(healed);
        }

        public void JoinFaction(IBelongToFactions.FactionsType type)
        {
            if (!Factions.Any(x => x == type)) Factions.Add(type);
        }

        public void LeaveFaction(IBelongToFactions.FactionsType type)
        {
            if (Factions.Any(x => x == type)) Factions.Remove(type);
        }

        public void LevelUp()
        {
            Level++;
            MaxHealth += MaxHealth * (Level * .3);
            Damage += Damage * (Level * .3);
            Health = MaxHealth;

        }

        public void Move(int metter)
        {
            if (!Alive) return;
            Metter = metter;
        }

        public bool IsInRange(IPosition unity)
        {
            return (Metter - unity.Metter) * -1 <= MaxRange;
        }

        public double DamageByLevelFactor(int damagedLevel)
        {
            var factor = 1d;

            if (Level >= damagedLevel + 5)
                factor = 1.5;
            else if (Level + 5 <= damagedLevel)
                factor = 0.5;

            return factor;
        }
    }
}
