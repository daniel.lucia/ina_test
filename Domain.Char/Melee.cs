﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Char
{
    public class Melee : Character
    {
        public Melee(int level = 1)
            :base(2, 500, level)
        {
        }
    }
}
