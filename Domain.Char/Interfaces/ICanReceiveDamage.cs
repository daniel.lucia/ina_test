﻿namespace Domain.Char
{
    public interface ICanReceiveDamage
    {
        public double MaxHealth { get; }
        public double Health { get; }
        public bool Alive { get; }

        public void ReceiveDamage(double damage);
    }
}
