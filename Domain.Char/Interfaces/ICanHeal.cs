﻿namespace Domain.Char
{
    public interface ICanHeal
    {
        public void Heal(ICanBeHealed healedUnity);
    }
}
