﻿using System.Collections.Generic;

namespace Domain.Char
{
    public interface IBelongToFactions
    {
        public enum FactionsType
        {
            NONE = 0,
            Faction1 = 1,
            Faction2 = 2,
            Faction3 = 3,
            Faction4 = 4,
        }

        public ICollection<FactionsType> Factions { get; }

        public void JoinFaction(FactionsType type);
        public void LeaveFaction(FactionsType type);

        public bool IsMyAlly(IBelongToFactions factionUnity);
    }
}
