﻿using System;

namespace Domain.Char
{
    public interface IIdentifier
    {
        public Guid Id { get; }
    }
}
