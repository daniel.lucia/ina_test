﻿namespace Domain.Char
{
    public interface ICanDoDamage
    {
        public double Damage { get; }
        public int MaxRange { get; }
        public void DoDamage(ICanReceiveDamage damagedUnity);

        public bool IsInRange(IPosition unity);
    }
}
