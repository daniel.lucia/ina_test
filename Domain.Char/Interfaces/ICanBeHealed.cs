﻿namespace Domain.Char
{
    public interface ICanBeHealed
    {
        public void Healed(double healed);
    }
}
