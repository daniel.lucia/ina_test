﻿namespace Domain.Char
{
    public interface ICanLevelUp
    {
        public int Level { get; }
        public void LevelUp();

        public double DamageByLevelFactor(int damagedLevel);
    }
}
