﻿using System;

namespace Domain.Char
{

    public class Tree : IIdentifier, ICanReceiveDamage, IPosition
    {
        public Guid Id => Guid.NewGuid();
        public double MaxHealth { get; private set; }
        public double Health { get; private set; }
        public int Metter { get; private set; }
        public bool Alive => Health > 0;

        public Tree()
        {
            Health = 2000;
            MaxHealth = Health;
        }

        public void ReceiveDamage(double damage)
        {
            Health -= damage;
            if (Health < 0) Health = 0;
        }
    }
}
